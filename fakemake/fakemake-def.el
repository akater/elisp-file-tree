;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'file-tree
  authors "Dima Akater"
  first-publication-year-as-string "2020"
  org-files-in-order '("file-tree-util"
                       "file-tree-macs"
                       "file-tree-core"
                       "file-tree")
  org-files-for-testing-in-order '("file-tree-tests")
  site-lisp-config-prefix "50"
  license "GPL-3")
