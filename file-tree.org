# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: file-tree
#+subtitle: “Main file” for =file-tree= Emacs Lisp package
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle file-tree.el :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) | DONE(d@)
#+todo: BUG(b@/!) | FIXED(x@)
#+todo: TEST(u) TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

=file-tree= is elisp package for manipulating symbolic file trees.

* Usage
See examples in the file [[file:file-tree-core.org]].

* Installation
** Gentoo
To install, please
#+begin_src sh :dir /sudo::/ :tangle no :results none
eselect repository enable akater
emerge app-emacs/file-tree
#+end_src

** Other systems
*** TODO Use package-vc (Emacs 29+)
- State "TODO"       from              [2023-06-27 Tue 12:45]
*** Install manually
Ensure the following dependencies are installed into your system-wide site-lisp dir:
- [[https://framagit.org/akater/elisp-mmxx-macros][mmxx-macros]]
- [[https://gitlab.com/akater/elisp-akater-misc][akater-misc]]

Clone ~file-tree~, switch to =release= branch.

~file-tree~ can be installed into system-wide directory =/usr/share/emacs/site-lisp= (or wherever your ~SITELISP~ envvar points to) with
#+begin_src sh :tangle no :results none
make && sudo make install
#+end_src
but it's best to define a package for your package manager to do this instead.

If your package manager can't do that, and you don't want to install with elevated permissions, do
#+begin_src sh :tangle no :results none
SITELISP=~/.config/emacs/lisp SITEETC=~/.config/emacs/etc make && \
DESTDIR=~/.config/emacs SITELISP=/lisp SITEETC=/etc make install
#+end_src
(you may simply eval this block with =C-c C-c=; it won't be informative if it fails but hopefully it'll work)

In any case, you'll have to add load-path to your config, as demonstrated in [[Suggested config]].

If you don't have a system-wide site-lisp directory where dependencies are found in versionless directories, you may evaluate [[elisp:(url-retrieve"https://framagit.org/akater/emacs-fakemake/-/raw/master/fakemake-make.org"(lambda(_ b &rest __)(eww-parse-headers)(let((rb (current-buffer))(start(point)))(with-current-buffer b(let((inhibit-read-only t))(erase-buffer)(insert-buffer-substring rb start)(goto-char(point-min))(eww-display-raw b'utf-8)(org-mode))(goto-char(org-babel-find-named-block"defun bootstrap-sitelisp"))(pop-to-buffer b)(recenter 1))))(list(get-buffer-create"https+org")))][this block]] and use the command defined there to create a symlink to org (and other packages that you use) in “site-lisp directory to fill” which you may specify.  That directory should be specified as ~SITELISP~ then.  If the link above is scary, find bootstrap-sitelisp manually [[https://framagit.org/akater/emacs-fakemake/-/raw/master/fakemake-make.org][here]].

** Notes for contiributors
If you have a local version of repository, compile with something like
#+begin_example sh :tangle no :results none
ORG_LOCAL_SOURCES_DIRECTORY="/home/contributor/src/elisp-file-tree"
#+end_example

This will link function definitions to your local Org files.

** Tests
If you want to run tests for ~file-tree~, ensure the following dependencies are installed:
- [[https://framagit.org/akater/org-run-tests][ORT]]
- [[https://framagit.org/akater/org-src-elisp-extras][org-src-elisp-extras]]
- [[https://framagit.org/akater/elisp-shmu][shmu]]

* Suggested config
#+begin_example elisp
(use-package file-tree :ensure nil :defer t
  ;; Note: on Gentoo, no need to specify load-path
  :load-path "/usr/share/emacs/site-lisp/file-tree"
  ;; or maybe
  ;; :load-path "~/.config/emacs/lisp/file-tree"
  )
#+end_example

* Features
#+begin_src elisp :results none
(require 'file-tree-core)
#+end_src
